# DVAnalysisUtilsRun2
------------------------------------
Author: Kazuki Motohashi  
Email: kazuki.motohashi[at]cern.ch

This is an analysis package for multitrack DV search in Run 2.  
Input files must be DxAODs of DV, i.e., the vertex container name is RPVSecVertices.

## Install
------------------------------------
On lxplus:

```sh
git clone ssh://git@gitlab.cern.ch:7999/kmotohas/DVAnalysisUtilsRun2.git
cd DVAnalysisUtilsRun2
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
rcSetup Base,2.3.34
rc find_packages
rc compile
```

### Install on your Mac
If you are a Mac user, you are recommended using Xcode to develop analysis codes within Rootcore framework.  
Rootcore can be built on your local machine following the software tutorial twiki.  
<https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialxAODEDM#On_your_local_machine_optional>  
A project file for Xcode can be generated using the CMakeGenerator package.  
<https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialxAODEDM#Using_an_IDE_to_develop_your_pac>

On Mac: (Assuming the RootCore is installed in your Mac)

```sh
git clone ssh://git@gitlab.cern.ch:7999/kmotohas/DVAnalysisUtilsRun2.git
cd DVAnalysisUtilsRun2
rcSetupLocal Base,2.3.34
rc find_packages
rc compile
rc checkout_pkg atlas-krasznaa/AODUpgrade/CMakeGenerator/trunk
mkdir IDE
cd IDE/
../CMakeGenerator/generateRootCoreCMakeProject.py
cmake -G Xcode
open RootCore.xcodeproj
```

## How To Use
------------------------------------

## Useful Links
------------------------------------
* [ATLAS Computing Portal Twiki](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/AtlasComputing)
* [EventLoop Twiki](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EventLoop)
* [SampleHandler Twiki](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SampleHandler)
* [CPAnalysisExamples Twiki](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/CPAnalysisExamples)
* [Developing CP Tools for the xAOD](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/DevelopingCPToolsForxAOD)
* [SUSYTools](https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/SUSYPhys/SUSYTools/trunk)
