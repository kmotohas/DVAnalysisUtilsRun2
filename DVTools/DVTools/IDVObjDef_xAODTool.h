// This file's extension implies that it's C, but it's really -*- C++ -*-.
#ifndef SUSYTOOLS_SUSYOBJDEF_XAODTOOL_H
#define SUSYTOOLS_SUSYOBJDEF_XAODTOOL_H

// Framework include(s):
#include "AsgTools/IAsgTool.h"

// EDM include(s):
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "xAODTracking/Vertex.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTau/TauJet.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetTypes.h"
#include "xAODMissingET/MissingET.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TrigDecisionTool/ChainGroup.h"

// Local include(s):
#include "PATInterfaces/CorrectionCode.h"
#include "PATInterfaces/ISystematicsTool.h"

namespace DV {

  enum SettingDataSource {
    Undefined = -1,
    Data,
    FullSim,
    AtlfastII
  };

  struct SystInfo{
    CP::SystematicSet systset;
    bool affectsKinematics;
    bool affectsWeights;
    unsigned int affectsType;
  };

  // Define a more compact enum than the IParticle one
  enum SystObjType {
    Unknown = 0,
    Jet,
    Egamma,
    Electron,
    Photon,
    Muon,
    Tau,
    BTag,
    MET_TST,
    MET_CST,
    MET_Track,
    EventWeight
  };

  // Helper method for affected objects
  static inline bool testAffectsObject(xAOD::Type::ObjectType type, unsigned int test) {
    switch(test) {
      case Jet:      return (type==xAOD::Type::Jet);
      case Egamma:   return (type==xAOD::Type::Electron||type==xAOD::Type::Photon);
      case Electron: return (type==xAOD::Type::Electron);
      case Photon:   return (type==xAOD::Type::Photon);
      case Muon:     return (type==xAOD::Type::Muon);
      case Tau:      return (type==xAOD::Type::Tau);
      case BTag:     return (type==xAOD::Type::BTag);
      default: break;
    }
    return false;
  }

  // Simple interface
  //
  // Following the design principles outlined in the TF3 recommendations.
  //
  //
  class IDVObjDef_xAODTool : public virtual asg::IAsgTool {

    // Declare the interface that the class provides
    ASG_TOOL_INTERFACE( DV::IDVObjDef_xAODTool )

    public:
      virtual StatusCode readConfig() = 0;

      // Apply the correction on a modifyable object
      virtual StatusCode FillMuon(xAOD::Muon& input, const float ptcut, const float etacut) = 0;
      virtual StatusCode FillJet(xAOD::Jet& input, const bool doCalib = true) = 0;
      virtual StatusCode FillTau(xAOD::TauJet& input) = 0;
      virtual StatusCode FillElectron(xAOD::Electron& input, const float etcut, const float etacut) = 0;
      virtual StatusCode FillPhoton(xAOD::Photon& input, const float ptcut, const float etacut) = 0;

      virtual const xAOD::Vertex* GetPrimVtx() = 0;

      virtual StatusCode GetJets(xAOD::JetContainer*& copy,xAOD::ShallowAuxContainer*& copyaux,const bool recordSG=false, const std::string& jetkey="") = 0;
      virtual StatusCode GetJetsSyst(const xAOD::JetContainer& calibjets,xAOD::JetContainer*& copy,xAOD::ShallowAuxContainer*& copyaux, const bool recordSG=false, const std::string& jetkey="") = 0;
      virtual StatusCode GetTaus(xAOD::TauJetContainer*& copy,xAOD::ShallowAuxContainer*& copyaux,const bool recordSG=false,const std::string& taukey="TauJets") = 0;
      virtual StatusCode GetMuons(xAOD::MuonContainer*& copy,xAOD::ShallowAuxContainer*& copyaux,const bool recordSG=false,const std::string& muonkey="Muons") = 0;
      virtual StatusCode GetElectrons(xAOD::ElectronContainer*& copy,xAOD::ShallowAuxContainer*& copyaux,const bool recordSG=false,const std::string& elekey="Electrons") = 0;
      virtual StatusCode GetPhotons(xAOD::PhotonContainer*& copy,xAOD::ShallowAuxContainer*& copyaux,const bool recordSG=false,const std::string& photonkey="Photons") = 0;
      virtual StatusCode GetMET(xAOD::MissingETContainer& met,
          const xAOD::JetContainer* jet,
          const xAOD::ElectronContainer* elec = 0,
          const xAOD::MuonContainer* muon = 0,
          const xAOD::PhotonContainer* gamma = 0,
          const xAOD::TauJetContainer* taujet = 0,
          bool doTST=true, bool doJVTCut=true,
          const xAOD::IParticleContainer* invis = 0) = 0;

      virtual StatusCode GetTrackMET(xAOD::MissingETContainer& met,
          const xAOD::JetContainer* jet,
          const xAOD::ElectronContainer* elec = 0,
          const xAOD::MuonContainer* muon = 0
          // const xAOD::PhotonContainer* gamma = 0,
          // const xAOD::TauJetContainer* taujet = 0,
          ) = 0;

      virtual StatusCode setRunNumber(const int run_number) = 0;

      virtual bool IsSignalJet(const xAOD::Jet& input,  const float ptcut, const float etacut, const float jvtcut) = 0;

      virtual bool IsBadJet(const xAOD::Jet& input, float jvtcut) = 0;

      virtual bool IsHighPtMuon(const xAOD::Muon& input) = 0;

      virtual bool IsSignalMuon(const xAOD::Muon& input, const float ptcut, const float d0sigcut, const float z0cut, const bool require_passedHighPtCuts) = 0;

      virtual bool IsSignalElectron(const xAOD::Electron& input, const float etcut, const float d0sigcut, const float z0cut) = 0;

      virtual bool IsCosmicMuon(const xAOD::Muon& input,const float z0cut, const float d0cut) = 0;

      virtual bool IsSignalTau(const xAOD::TauJet& input, const float ptcut, const float etacut) = 0;

      virtual bool IsBadMuon(const xAOD::Muon& input, const float qopcut) = 0;

      virtual bool IsSignalPhoton(const xAOD::Photon& input, const float ptcut) = 0;

      //rel20 0.77 eff value (22/6/15) from https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BTaggingBenchmarks#MV2c20_tagger_AntiKt4EMTopoJets   
      //assumes JVT>0.64 working point
      virtual bool IsBJet(const xAOD::Jet& input) = 0;

      virtual float BtagSF(const xAOD::JetContainer* goodJets) = 0;

      virtual float GetSignalMuonSF(const xAOD::Muon& mu, const bool recoSF = true, const bool isoSF = true) = 0;

      virtual float GetSignalElecSF(const xAOD::Electron& el, const bool recoSF = true, const bool idSF = true, const bool triggerSF = true) = 0;

      virtual double GetTotalMuonSF(const xAOD::MuonContainer& muons, const bool recoSF = true, const bool isoSF = true, const std::string& trigExpr = "HLT_mu20_iloose_L1MU15_OR_HLT_mu50") = 0;

      virtual float GetTotalElectronSF(const xAOD::ElectronContainer& electrons, const bool recoSF = true, const bool idSF = true, const bool triggerSF = true) = 0;

      virtual float GetSignalTauSF(const xAOD::TauJet& tau) = 0;

      virtual double GetSignalPhotonSF(const xAOD::Photon& ph) = 0;

      virtual bool IsTrigPassed(const std::string&) = 0;

      virtual bool IsTrigMatched(const xAOD::Muon *mu, const std::string&) = 0;

      virtual bool IsTrigMatched(const xAOD::Egamma *eg, const std::string&) = 0;

      virtual bool IsTrigMatched(const xAOD::Photon *eg, const std::string&) = 0;

      virtual bool IsTrigMatched(const xAOD::TauJet *tau, const std::string&) = 0;

      virtual bool IsTrigMatched(const xAOD::TauJet *tau1, const xAOD::TauJet *tau2, const std::string&) = 0; 

      virtual bool IsTrigMatched(const xAOD::Muon *mu1, const xAOD::Muon *mu2, const std::string&) = 0; 

      virtual float GetTrigPrescale(const std::string&) = 0;

      virtual const Trig::ChainGroup* GetTrigChainGroup(const std::string&) = 0;

      virtual float GetPileupWeight() = 0;

      virtual ULong64_t GetPileupWeightHash( ) = 0;

      virtual float GetDataWeight(const std::string&) = 0;

      virtual StatusCode OverlapRemoval(const xAOD::ElectronContainer *electrons, const xAOD::MuonContainer *muons, const xAOD::JetContainer *jets,
          const bool useSignalLeptons = false, const bool useIsolLeptons=false, const bool doBjetOR=false,
          const double dRejet = 0.2, const double dRjetmu = 0.4, const double dRjete = 0.4, const double dRemu = 0.01, const double dRee = 0.05) = 0;

      virtual StatusCode OverlapRemoval(const xAOD::ElectronContainer *electrons, const xAOD::MuonContainer *muons, const xAOD::JetContainer *jets, const xAOD::PhotonContainer *photons,
          const bool useSignalObjects = false, const bool useIsolObjects=false, const bool doBjetOR=false,
          const double dRejet = 0.2, const double dRjetmu = 0.4, const double dRjete = 0.4, const double dRemu = 0.01, const double dRee = 0.05, double dRphjet=0.4, double dReph = 0.4, double dRmuph = 0.4) = 0;

      virtual CP::SystematicCode resetSystematics() = 0;

      virtual CP::SystematicCode applySystematicVariation( const CP::SystematicSet& systConfig ) = 0;

      virtual StatusCode FindSusyHP(const xAOD::TruthParticleContainer *truthP, int& pdgid1, int& pdgid2) = 0;

      virtual std::vector<DV::SystInfo> getSystInfoList() = 0;

  }; // class IDVObjDef_xAODTool

} // namespace DV

#endif // DVTOOLS_SUSYOBJDEF_XAODTOOL_H
