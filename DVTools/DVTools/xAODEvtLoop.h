#ifndef DVTools_xAODEvtLoop_H
#define DVTools_xAODEvtLoop_H

#include <EventLoop/Algorithm.h>

// xAOD
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
// include files for using the trigger tools
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
// ROOT
#include "TList.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TMath.h"
// std
#include <vector>

class xAODEvtLoop : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

  bool m_isMC; //!
  std::string m_configFile; //!
  std::string m_longlivedParticle; //!
  std::string m_vertexContainer; //!
  std::string m_trackContainer; //!
  std::string m_jetContainer; //!
  bool m_doVertexingEfficiency; //!
  bool m_doTrackingEfficiency; //!
  bool m_doDESDFilterStudy; //!
  bool m_doTriggerStudy; //!
  double m_jetPtCut; //!
  double m_jetEtaCut; //!
  double m_MeffCut; //!
  double m_METoverMeffLowerCut; //!
  double m_METoverMeffUpperCut; //!
  double m_METCut; //!
  std::vector<std::string> m_triggerCollection; //!
  std::string m_triggerCollectionName; //!
  
  xAOD::TEvent *m_event; //!
  unsigned long m_eventCounter; //!
  
  // debug
  int m_nEventsChi0_2 = 0;
  int m_nEventsChi0_1 = 0;
  int m_nEventsChi0_0 = 0;

  // trigger tools member variables
  Trig::TrigDecisionTool *m_trigDecisionTool; //!
  TrigConf::xAODConfigTool *m_trigConfigTool; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // histgrams/graphs
  TH1D *h_recoVertPosR; //!
  TH1D *h_truthVertPosR; //!
  TGraphAsymmErrors *g_vertEffR; //!
  TH1D *h_recoTrackD0; //!
  TH1D *h_truthTrackD0; //!
  TGraphAsymmErrors *g_trackEffD0; //!
  TH1D *h_recoTrackZ0; //!
  TH1D *h_truthTrackZ0; //!
  TGraphAsymmErrors *g_trackEffZ0; //!
  TH1D *h_JetPt; //!
  TH1D *h_nJets; //!
  TH1D *h_nJets10; //!
  TH1D *h_nJets20; //!
  TH1D *h_Meff; //!
  TH1D *h_MET_Calo; //!
  TH1D *h_METoverMeff; //!
  TH2D *h_METvsMeff; //!
  TH2D *h_METvsChi0Pt; //!
  TH1D *h_chi0pt; //!
  TH1D *h_HLT_xe80; //!
  TH1D *h_HLT_xe100; //!
  TH1D *h_HLT_xe100_tc_lcw; //!
  TH1D *h_HLT_xe100_tc_lcw_wEFMu; //!
  TH1D *h_HLT_xe100_wEFMu; //!
  TH1D *h_HLT_xe100_OR; //!
  TH1D *h_HLT_j100_xe80; //!
  TH2D *h_HLT_jetPlusMETvsMET; //!

  // this is a standard constructor
  xAODEvtLoop ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();
  
  EL::StatusCode readConfig();
  bool isRhadron(int pdgid){
    //pdgid ==  1000021 // gluino
    if(pdgid ==  1000993 || // glueball
       pdgid ==  1009213 ||
       pdgid ==  1009313 ||
       pdgid ==  1009323 ||
       pdgid ==  1009113 ||
       pdgid ==  1009223 ||
       pdgid ==  1009333 ||
       pdgid ==  1091114 ||
       pdgid ==  1092114 ||
       pdgid ==  1092214 ||
       pdgid ==  1092224 ||
       pdgid ==  1093114 ||
       pdgid ==  1093214 ||
       pdgid ==  1093224 ||
       pdgid ==  1093314 ||
       pdgid ==  1093324 ||
       pdgid ==  1093334 ||
       pdgid == -1009213 ||
       pdgid == -1009313 ||
       pdgid == -1009323 ||
       pdgid == -1091114 ||
       pdgid == -1092114 ||
       pdgid == -1092214 ||
       pdgid == -1092224 ||
       pdgid == -1093114 ||
       pdgid == -1093214 ||
       pdgid == -1093224 ||
       pdgid == -1093314 ||
       pdgid == -1093324 ||
       pdgid == -1093334) {
      //pdgid ==  1000039    // Gravitino
      //pdgid ==  1000022   // ~chi_10
      return true;
    } else {
      return false;
    }
  };
  bool isLightestNeutralino(int pdgid){
    if(pdgid == 1000022) {
      return true;
    } else {
      return false;
    }
  };
  bool isLLP(int pdgid) {
    if (m_longlivedParticle == "gluino") {
      return isRhadron(pdgid);
    } else if (m_longlivedParticle == "bino") {
      return isLightestNeutralino(pdgid);
    } else {
      Error("isLLP()", "longlivedParticle has invalid name");
      return false;
    }
  };
  double getDistance2D(double x1, double y1, double x2, double y2){
    return TMath::Sqrt(TMath::Power(x1 - x2, 2)
                       + TMath::Power(y1 - y2, 2));
  };
  double getDistance3D(double x1, double y1, double z1, double x2, double y2, double z2){
    return TMath::Sqrt(TMath::Power(x1 - x2, 2)
                       + TMath::Power(y1 - y2, 2)
                       + TMath::Power(z1 - z2, 2));
  };

  // this is needed to distribute the algorithm to the workers
  ClassDef(xAODEvtLoop, 1);
  
};

#endif
