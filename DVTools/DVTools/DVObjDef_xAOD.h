// This file's extension implies that it's C, but it's really -*- C++ -*-.
// $Id$
/**
 * @file DVTools/DVTools/DVObjDef_xAOD.h
 * @author Kazuki Motohashi <kazuki.motohashi@cern.ch>
 * @date Aug, 2015
 * @brief Baseline DV object definitions.
 */

#ifndef DVTOOLS_DVOBJDEF_XAOD_H
#define DVTOOLS_DVOBJDEF_XAOD_H

// Framework include(s):
#include "AsgTools/AsgMetadataTool.h"

#include "DVTools/IDVObjDef_xAODTool.h"

///////////////////////// -*- C++ -*- /////////////////////////////
#include "PATInterfaces/SystematicCode.h"
#include "PATInterfaces/SystematicSet.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicVariation.h"

#include "AsgTools/ToolHandle.h"

// Tool interfaces

class IJetCalibrationTool;
class IJERTool;
class IJERSmearingTool;
class ICPJetUncertaintiesTool;
class IJetSelector;
class IJetUpdateJvt;

class IAsgElectronEfficiencyCorrectionTool;
class IAsgElectronEfficiencyCorrectionTool;
class IAsgElectronEfficiencyCorrectionTool;

class IAsgElectronLikelihoodTool;
class IAsgElectronIsEMSelector;
class IAsgPhotonIsEMSelector;
class IAsgPhotonEfficiencyCorrectionTool;
class IElectronPhotonShowerShapeFudgeTool;

class IBTaggingEfficiencyTool;
class IBTaggingSelectionTool;

class IMETMaker;
class IMETSystematicsTool;

namespace CP {
  class IMuonSelectionTool;
  class IMuonCalibrationAndSmearingTool;
  class IMuonEfficiencyScaleFactors;
  class IMuonTriggerScaleFactors;

  class IEgammaCalibrationAndSmearingTool;
  class IIsolationSelectionTool;
  class IPileupReweightingTool;
}

namespace TauAnalysisTools {
  class ITauSelectionTool;
  class ITauSmearingTool;
  class ITauEfficiencyCorrectionsTool;
}

namespace TrigConf{
  class ITrigConfigTool;
}
namespace Trig {
  class TrigDecisionTool;
  class ITrigEgammaMatchingTool;
  class ITrigMuonMatching;
  class ITrigTauMatchingTool;
}

namespace DV {

  //  class DVObjDef;

  class DVObjDef_xAOD : public virtual IDVObjDef_xAODTool,
  public asg::AsgMetadataTool {

    // Create a proper constructor for Athena
    ASG_TOOL_CLASS( DVObjDef_xAOD, DV::IDVObjDef_xAODTool)

    public:
      DVObjDef_xAOD( const std::string& name );
      ~DVObjDef_xAOD();

      // Function initialising the tool
      StatusCode initialize();

      StatusCode DVToolsInit();
      StatusCode readConfig();

      bool isData() {return m_dataSource==Data;}
      bool isAtlfast() {return m_dataSource==AtlfastII;}

      // Apply the correction on a modifyable object
      StatusCode FillJet(xAOD::Jet& input, const bool doCalib = true);
      StatusCode FillTau(xAOD::TauJet& input);
      StatusCode FillMuon(xAOD::Muon& input, const float ptcut, const float etacut);
      StatusCode FillElectron(xAOD::Electron& input, const float etcut, const float etacut);
      StatusCode FillPhoton(xAOD::Photon& input, const float ptcut, const float etacut);

      const xAOD::Vertex* GetPrimVtx();

      StatusCode GetJets(xAOD::JetContainer*& copy,xAOD::ShallowAuxContainer*& copyaux,const bool recordSG=false, const std::string& jetkey="");
      StatusCode GetJetsSyst(const xAOD::JetContainer& calibjets,xAOD::JetContainer*& copy,xAOD::ShallowAuxContainer*& copyaux, const bool recordSG=false, const std::string& jetkey="");
      StatusCode GetTaus(xAOD::TauJetContainer*& copy,xAOD::ShallowAuxContainer*& copyaux,const bool recordSG=false,const std::string& taukey="TauJets");
      StatusCode GetMuons(xAOD::MuonContainer*& copy,xAOD::ShallowAuxContainer*& copyaux,const bool recordSG=false,const std::string& muonkey="Muons");
      StatusCode GetElectrons(xAOD::ElectronContainer*& copy,xAOD::ShallowAuxContainer*& copyaux,const bool recordSG=false,const std::string& elekey="Electrons");
      StatusCode GetPhotons(xAOD::PhotonContainer*& copy,xAOD::ShallowAuxContainer*& copyaux,const bool recordSG=false,const std::string& photonkey="Photons");
      StatusCode GetMET(xAOD::MissingETContainer& met,
          const xAOD::JetContainer* jet,
          const xAOD::ElectronContainer* elec = 0,
          const xAOD::MuonContainer* muon = 0,
          const xAOD::PhotonContainer* gamma = 0,
          const xAOD::TauJetContainer* taujet = 0,
          bool doTST=true, bool doJVTCut=true,
          const xAOD::IParticleContainer* invis = 0);

      StatusCode GetTrackMET(xAOD::MissingETContainer& met,
          const xAOD::JetContainer* jet,
          const xAOD::ElectronContainer* elec = 0,
          const xAOD::MuonContainer* muon = 0
          // const xAOD::PhotonContainer* gamma = 0,
          // const xAOD::TauJetContainer* taujet = 0,
          );

      StatusCode setRunNumber(const int run_number);

      bool IsSignalJet(const xAOD::Jet& input, const float ptcut, const float etacut, const float jvtcut);

      bool IsBadJet(const xAOD::Jet& input, float jvtcut);

      bool IsSignalMuon(const xAOD::Muon& input, const float ptcut, const float d0sigcut, const float z0cut, const bool require_passedHighPtCuts);

      bool IsSignalElectron(const xAOD::Electron& input, const float etcut, const float d0sigcut, const float z0cut);

      bool IsCosmicMuon(const xAOD::Muon& input,const float z0cut, const float d0cut);

      bool IsHighPtMuon(const xAOD::Muon& input); // See https://indico.cern.ch/event/371499/contribution/1/material/slides/0.pdf and https://indico.cern.ch/event/397325/contribution/19/material/slides/0.pdf and https://twiki.cern.ch/twiki/bin/view/Atlas/MuonSelectionTool

      bool IsSignalTau(const xAOD::TauJet& input, const float ptcut, const float etacut);

      bool IsBadMuon(const xAOD::Muon& input, const float qopcut);

      bool IsSignalPhoton(const xAOD::Photon& input, const float ptcut);

      //rel20 0.77 eff value (22/6/15) from https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BTaggingBenchmarks#MV2c20_tagger_AntiKt4EMTopoJets   
      //assumes JVT>0.64 working point
      bool IsBJet(const xAOD::Jet& input);

      float BtagSF(const xAOD::JetContainer* goodJets);

      float GetSignalMuonSF(const xAOD::Muon& mu, const bool recoSF = true, const bool isoSF = true);

      float GetSignalElecSF(const xAOD::Electron& el, const bool recoSF = true, const bool idSF = true, const bool triggerSF = true);

      float GetSignalTauSF(const xAOD::TauJet& tau);

      double GetSignalPhotonSF(const xAOD::Photon& ph);

      double GetTotalMuonSF(const xAOD::MuonContainer& muons, const bool recoSF = true, const bool isoSF = true, const std::string& trigExpr = "HLT_mu20_iloose_L1MU15_OR_HLT_mu50");

      float GetTotalElectronSF(const xAOD::ElectronContainer& electrons, const bool recoSF = true, const bool idSF = true, const bool triggerSF = true);

      bool IsTrigPassed(const std::string&);

      bool IsTrigMatched(const xAOD::Egamma *eg, const std::string&);

      bool IsTrigMatched(const xAOD::Photon *eg, const std::string&);

      bool IsTrigMatched(const xAOD::Muon *mu, const std::string&);

      bool IsTrigMatched(const xAOD::TauJet *tau, const std::string&);

      bool IsTrigMatched(const xAOD::TauJet *tau1, const xAOD::TauJet *tau2, const std::string&); 

      bool IsTrigMatched(const xAOD::Muon *mu1, const xAOD::Muon *mu2, const std::string&);

      float GetTrigPrescale(const std::string&);

      const Trig::ChainGroup* GetTrigChainGroup(const std::string&);

      float GetPileupWeight();

      ULong64_t GetPileupWeightHash();

      float GetDataWeight(const std::string&);

      StatusCode OverlapRemoval(const xAOD::ElectronContainer *electrons, const xAOD::MuonContainer *muons, const xAOD::JetContainer *jets,
          const bool useSignalLeptons = false, const bool useIsolLeptons=false, const bool doBjetOR=false,
          const double dRejet = 0.2, const double dRjetmu = 0.4, const double dRjete = 0.4, const double dRemu = 0.01, const double dRee = 0.05);

      StatusCode OverlapRemoval(const xAOD::ElectronContainer *electrons, const xAOD::MuonContainer *muons, const xAOD::JetContainer *jets, const xAOD::PhotonContainer *photons,
          const bool useSignalObjects = false, const bool useIsolObjects=false, const bool doBjetOR=false,
          const double dRejet = 0.2, const double dRjetmu = 0.4, const double dRjete = 0.4, const double dRemu = 0.01, const double dRee = 0.05, double dRphjet=0.4, double dReph = 0.4, double dRmuph = 0.4);

      CP::SystematicCode resetSystematics();

      CP::SystematicCode applySystematicVariation( const CP::SystematicSet& systConfig );

      StatusCode FindSusyHP(const xAOD::TruthParticleContainer *truthP, int& pdgid1, int& pdgid2);

      static bool FindSusyHardProc(const xAOD::TruthParticleContainer *truthP, int& pdgid1, int& pdgid2);

      std::vector<DV::SystInfo> getSystInfoList();


    protected:

#ifdef XAOD_STANDALONE // more convenient for property setting
      SettingDataSource m_dataSource;
      xAOD::JetInput::Type m_jetInputType; // permit switching between LC, PFlow, EM jets
      xAOD::Muon::Quality m_muId;
#else
      int m_dataSource;
      int m_jetInputType;
      int m_muId;
#endif
      std::string m_configFile;

      bool m_is25ns;
      bool m_doJetArea, m_doJetGSC;
      int m_jesNPset;
      bool m_debug;
      std::string m_badJetCut;

      bool m_tool_init;
      bool m_subtool_init;

      std::string m_eleTerm    ;
      std::string m_gammaTerm  ;
      std::string m_tauTerm    ;
      std::string m_jetTerm    ;
      std::string m_muonTerm   ;
      std::string m_inputMETSuffix;
      std::string m_inputMETMap;
      std::string m_inputMETCore;
      std::string m_inputMETRef;
      std::string m_outMETTerm;
      bool m_trkMETsyst;
      bool m_caloMETsyst;

      int m_prwDefaultChannel;
      std::vector<std::string> m_prwConfFiles;
      std::vector<std::string> m_prwLcalcFiles;
      float m_muUncert;

      std::string m_eleId;
      std::string m_eleIdBaseline;
      std::string m_phId;
      std::string m_tauId;
      std::string m_eleIso_WP;
      std::string m_phIso_WP;
      std::string m_muIso_WP;
      std::string m_BtagWP;
      std::string m_BtagWP_OR;
      bool m_doIsoSignal;

      //configurable cuts here
      double m_eleBaselinePt;
      double m_elePt;
      double m_eleEta;
      double m_eled0sig;
      double m_elez0;

      double m_muBaselinePt;
      double m_muPt;
      double m_muEta;
      double m_mud0sig;
      double m_muz0;
      bool m_murequirepassedHighPtCuts;

      double m_muCosmicz0;
      double m_muCosmicd0;

      double m_badmuQoverP;

      double m_photonBaselinePt;
      double m_photonEta;
      double m_photonPt;

      double m_tauPt;
      double m_tauEta;

      double m_jetPt;
      double m_jetEta;
      double m_jetJvt;
      double m_badjetJvt;

      //
      std::string m_defaultJets;
      std::string m_currentSyst;

      ToolHandle<IJetCalibrationTool> m_jetCalibTool;
      ToolHandle<IJERTool> m_jerTool;
      ToolHandle<IJERSmearingTool> m_jerSmearingTool;
      ToolHandle<ICPJetUncertaintiesTool> m_jetUncertaintiesTool;
      ToolHandle<IJetSelector> m_jetCleaningTool;
      ToolHandle<IJetUpdateJvt> m_jvt;
      //
      ToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool;
      ToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool;
      ToolHandle<CP::IMuonEfficiencyScaleFactors> m_muonEfficiencySFTool;
      ToolHandle<CP::IMuonEfficiencyScaleFactors> m_muonIsolationSFTool;
      ToolHandle<CP::IMuonTriggerScaleFactors> m_muonTriggerSFTool;
      //
      ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_elecEfficiencySFTool_reco;
      ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_elecEfficiencySFTool_id;
      ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_elecEfficiencySFTool_trig;
      ToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_egammaCalibTool;
      ToolHandle<IAsgElectronLikelihoodTool> m_elecSelLikelihood;
      ToolHandle<IAsgElectronIsEMSelector>   m_elecSelIsEM;
      ToolHandle<IAsgElectronLikelihoodTool> m_elecSelLikelihoodBaseline;
      ToolHandle<IAsgElectronIsEMSelector>   m_elecSelIsEMBaseline;
      ToolHandle<IAsgPhotonIsEMSelector>     m_photonSelIsEM;
      ToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_photonEfficiencySFTool;
      ToolHandle<IElectronPhotonShowerShapeFudgeTool> m_electronPhotonShowerShapeFudgeTool;
      //
      ToolHandle<TauAnalysisTools::ITauSelectionTool> m_tauSelTool;
      ToolHandle<TauAnalysisTools::ITauSmearingTool> m_tauSmearingTool;
      ToolHandle<TauAnalysisTools::ITauEfficiencyCorrectionsTool> m_tauEffTool;
      //
      ToolHandle<IBTaggingEfficiencyTool> m_btagEffTool;
      //////ToolHandle<IBTaggingSelectionTool> m_btagSelTool;
      //////ToolHandle<IBTaggingSelectionTool> m_btagSelTool_OR;
      //
      ToolHandle<IMETMaker> m_metMaker;
      ToolHandle<IMETSystematicsTool> m_metSystTool;
      //
      ToolHandle<TrigConf::ITrigConfigTool> m_trigConf;
      ToolHandle<Trig::TrigDecisionTool> m_trigDec;
      ToolHandle<Trig::ITrigEgammaMatchingTool> m_EgammaMatchTool;
      ToolHandle<Trig::ITrigMuonMatching> m_MuonMatchTool;
      ToolHandle<Trig::ITrigTauMatchingTool> m_TauMatchTool;   
      //
      ToolHandle<CP::IIsolationSelectionTool> m_isoTool;
      //
      ToolHandle<CP::IPileupReweightingTool> m_prwTool;

  }; // Class DVObjDef_xAOD

} // namespace DV


#endif // not DVTOOLS_DVOBJDEF_XAOD_H