# echo "setup DVTools DVTools-00-00-00 in /afs/cern.ch/work/k/kmotohas/DisplacedVertex/DVAnalysisUtilsRun2"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc48-opt/2.3.21/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtDVToolstempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtDVToolstempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=DVTools -version=DVTools-00-00-00 -path=/afs/cern.ch/work/k/kmotohas/DisplacedVertex/DVAnalysisUtilsRun2  -no_cleanup $* >${cmtDVToolstempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=DVTools -version=DVTools-00-00-00 -path=/afs/cern.ch/work/k/kmotohas/DisplacedVertex/DVAnalysisUtilsRun2  -no_cleanup $* >${cmtDVToolstempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtDVToolstempfile}
  unset cmtDVToolstempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtDVToolstempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtDVToolstempfile}
unset cmtDVToolstempfile
exit $cmtsetupstatus

