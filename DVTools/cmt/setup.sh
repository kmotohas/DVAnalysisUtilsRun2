# echo "setup DVTools DVTools-00-00-00 in /afs/cern.ch/work/k/kmotohas/DisplacedVertex/DVAnalysisUtilsRun2"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc48-opt/2.3.21/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtDVToolstempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtDVToolstempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=DVTools -version=DVTools-00-00-00 -path=/afs/cern.ch/work/k/kmotohas/DisplacedVertex/DVAnalysisUtilsRun2  -no_cleanup $* >${cmtDVToolstempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=DVTools -version=DVTools-00-00-00 -path=/afs/cern.ch/work/k/kmotohas/DisplacedVertex/DVAnalysisUtilsRun2  -no_cleanup $* >${cmtDVToolstempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtDVToolstempfile}
  unset cmtDVToolstempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtDVToolstempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtDVToolstempfile}
unset cmtDVToolstempfile
return $cmtsetupstatus

