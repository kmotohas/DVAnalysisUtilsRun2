// Local include(s):
#include "DVTools/DVObjDef_xAOD.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETComposition.h"
#include "xAODBase/IParticleHelpers.h"

#include "EventPrimitives/EventPrimitivesHelpers.h"
#include "xAODPrimitives/IsolationType.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "AthContainers/ConstDataVector.h"
#include "PATInterfaces/SystematicsUtil.h"

// Tool interfaces
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetInterface/IJetSelector.h"
#include "JetResolution/IJERTool.h"
#include "JetResolution/IJERSmearingTool.h"
#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
#include "JetInterface/IJetUpdateJvt.h"
//
#include "MuonMomentumCorrections/IMuonCalibrationAndSmearingTool.h"
#include "MuonEfficiencyCorrections/IMuonEfficiencyScaleFactors.h"
#include "MuonSelectorTools/IMuonSelectionTool.h"
#include "MuonEfficiencyCorrections/IMuonTriggerScaleFactors.h"
//
#include "ElectronPhotonFourMomentumCorrection/IEgammaCalibrationAndSmearingTool.h"
#include "ElectronEfficiencyCorrection/IAsgElectronEfficiencyCorrectionTool.h"
#include "ElectronPhotonSelectorTools/IAsgElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/IAsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/IAsgElectronLikelihoodTool.h"
#include "ElectronPhotonShowerShapeFudgeTool/IElectronPhotonShowerShapeFudgeTool.h"
#include "PhotonEfficiencyCorrection/IAsgPhotonEfficiencyCorrectionTool.h"
//
#include "TauAnalysisTools/ITauSelectionTool.h"
#include "TauAnalysisTools/ITauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/ITauSmearingTool.h"
//
#include "xAODBTaggingEfficiency/IBTaggingEfficiencyTool.h"
////////#include "xAODBTaggingEfficiency/IBTaggingSelectionTool.h"
//
#include "METInterface/IMETMaker.h"
#include "METInterface/IMETSystematicsTool.h"
// xAOD trigger
#include "TrigConfInterfaces/ITrigConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigEgammaMatchingTool/ITrigEgammaMatchingTool.h"
#include "TrigMuonMatching/ITrigMuonMatching.h"
#include "TrigTauMatching/ITrigTauMatching.h"
//
#include "IsolationSelection/IIsolationSelectionTool.h"
//
#include "PileupReweighting/IPileupReweightingTool.h"
//
#include "TEnv.h"
#include "PathResolver/PathResolver.h"

#if 0
namespace DV {

  /*
	using namespace Trig;
	using namespace TrigConf;
	using namespace xAOD;

	static SG::AuxElement::Decorator<char> dec_baseline("baseline");
	static SG::AuxElement::Decorator<char> dec_signal("signal");
	static SG::AuxElement::Decorator<char> dec_isol("isol");
	static SG::AuxElement::Decorator<char> dec_passOR("passOR");
	static SG::AuxElement::Decorator<char> dec_bad("bad");
	static SG::AuxElement::Decorator<char> dec_bjet("bjet");
	static SG::AuxElement::Decorator<char> dec_bjet_loose("bjet_loose");
	static SG::AuxElement::Decorator<char> dec_cosmic("cosmic");
	static SG::AuxElement::Decorator<char> dec_passedHighPtCuts("passedHighPtCuts");

	static SG::AuxElement::Accessor<float>  acc_jvt("Jvt");
	static SG::AuxElement::ConstAccessor<float> cacc_jvt("Jvt");
	static SG::AuxElement::Decorator<double> dec_effscalefact("effscalefact");
   */

	DVObjDef_xAOD::DVObjDef_xAOD( const std::string& name )
	  : asg::AsgMetadataTool( name )
  /*
	    m_dataSource(Undefined),
	    m_debug(false),
	    m_tool_init(false),
	    m_subtool_init(false),
	    m_currentSyst(""),
	    m_jetCalibTool(""),
	    m_jerTool(""),
	    m_jerSmearingTool(""),
	    m_jetUncertaintiesTool(""),
	    m_jetCleaningTool(""),
	    m_jvt(""),
	    m_muonSelectionTool(""),
	    m_muonCalibrationAndSmearingTool(""),
	    m_muonEfficiencySFTool(""),
	    m_muonIsolationSFTool(""),
	    m_muonTriggerSFTool(""),
	    m_elecEfficiencySFTool_reco(""),
	    m_elecEfficiencySFTool_id(""),
	    m_elecEfficiencySFTool_trig(""),
	    m_egammaCalibTool(""),
	    m_elecSelLikelihood(""),
	    m_elecSelIsEM(""),
	    m_elecSelLikelihoodBaseline(""),
	    m_elecSelIsEMBaseline(""),
	    m_photonSelIsEM(""),
	    m_photonEfficiencySFTool(""),
	    m_electronPhotonShowerShapeFudgeTool(""),
	    m_tauSelTool(""),
	    m_tauSmearingTool(""),
	    m_tauEffTool(""),
	    m_btagEffTool(""),
	    ////////m_btagSelTool(""),
	    ////////m_btagSelTool_OR(""),
	    m_metMaker(""),
	    m_metSystTool(""),
	    m_trigConf(""),
	    m_trigDec(""),
	    m_EgammaMatchTool(""),
	    m_MuonMatchTool(""),
	    m_TauMatchTool(""),
	    m_isoTool(""),
	    m_prwTool("")
   **/
	{

    /**
	  declareProperty( "DataSource", m_dataSource = Undefined );
	  declareProperty( "DebugMode",  m_debug );
	  declareProperty( "ConfigFile",  m_configFile );
	  declareProperty( "Is25ns",     m_is25ns = true );

	  declareProperty( "EleId",         m_eleId = "TightLH");
	  declareProperty( "EleIdBaseline", m_eleIdBaseline = "LooseLH");
	  declareProperty( "PhotonId",      m_phId = "Tight");
	  declareProperty( "MuId",          m_muId =  xAOD::Muon::Medium);  //MuonQuality: Tight, Medium, Loose, VeryLoose
	  declareProperty( "TauId",         m_tauId = "Medium");
	  declareProperty( "BtagWP",        m_BtagWP = "FixedCutBEff_77" );
	  declareProperty( "BtagWP_OR",     m_BtagWP_OR = "FixedCutBEff_80" );

	  declareProperty( "EleIsoWP",    m_eleIso_WP = "GradientLoose");
	  declareProperty( "MuIsoWP",     m_muIso_WP  = "GradientLoose");
	  declareProperty( "PhotonIsoWP", m_phIso_WP  = "Cone40");
	  declareProperty( "RequireIsoSignal", m_doIsoSignal = true );

	  declareProperty( "BadJetCut",      m_badJetCut = "LooseBad" );
	  declareProperty( "JetInputType",   m_jetInputType = xAOD::JetInput::EMTopo );
	  declareProperty( "DoJetAreaCalib", m_doJetArea = true );
	  declareProperty( "DoJetGSCCalib",  m_doJetGSC = true );
	  declareProperty( "JESNuisanceParameterSet", m_jesNPset = 0 );

	  declareProperty( "METEleTerm",     m_eleTerm   = "RefEle"   );
	  declareProperty( "METGammaTerm",   m_gammaTerm = "RefGamma" );
	  declareProperty( "METTauTerm",     m_tauTerm   = "RefTau"   );
	  declareProperty( "METJetTerm",     m_jetTerm   = "RefJet"   );
	  declareProperty( "METMuonTerm",    m_muonTerm  = "Muons"    );
	  declareProperty( "METOutputTerm",  m_outMETTerm = "Final"   );
	  declareProperty( "METDoTrkSyst",   m_trkMETsyst  = true     );
	  declareProperty( "METDoCaloSyst",  m_caloMETsyst = false    );

	  declareProperty( "PRWDefaultChannel", m_prwDefaultChannel = 0 );
	  declareProperty( "PRWConfigFiles",    m_prwConfFiles );
	  declareProperty( "PRWLumiCalcFiles",  m_prwLcalcFiles );
	  declareProperty( "PRWMuUncertainty",  m_muUncert = 0.2);

	  declareProperty( "JetCalibTool", m_jetCalibTool);
	  declareProperty( "JERTool", m_jerTool);
	  declareProperty( "JERSmearingTool", m_jerSmearingTool);
	  declareProperty( "JetUncertaintiesTool", m_jetUncertaintiesTool);
	  declareProperty( "JetCleaningTool", m_jetCleaningTool);
	  //
	  declareProperty( "MuonSelectionTool", m_muonSelectionTool);
	  declareProperty( "MuonCalibrationAndSmearingTool", m_muonCalibrationAndSmearingTool);
	  declareProperty( "MuonEfficiencyScaleFactorsTool", m_muonEfficiencySFTool);
	  declareProperty( "MuonIsolationScaleFactorsTool", m_muonIsolationSFTool);
	  declareProperty( "MuonTriggerScaleFactorsTool", m_muonTriggerSFTool);
	  //
	  declareProperty( "ElectronEfficiencyCorrectionTool_reco", m_elecEfficiencySFTool_reco);
	  declareProperty( "ElectronEfficiencyCorrectionTool_trig", m_elecEfficiencySFTool_trig);
	  declareProperty( "ElectronEfficiencyCorrectionTool_id"  , m_elecEfficiencySFTool_id);
	  declareProperty( "ElectronLikelihoodTool" , m_elecSelLikelihood);
	  declareProperty( "ElectronIsEMSelector" , m_elecSelIsEM);
	  declareProperty( "ElectronLikelihoodToolBaseline" , m_elecSelLikelihoodBaseline);
	  declareProperty( "ElectronIsEMSelectorBaseline" , m_elecSelIsEMBaseline);
	  //
	  declareProperty( "PhotonIsEMSelector" , m_photonSelIsEM);
	  declareProperty( "PhotonEfficiencyCorrectionTool", m_photonEfficiencySFTool);
	  declareProperty( "PhotonShowerShapeFudgeTool", m_electronPhotonShowerShapeFudgeTool);
	  //
	  declareProperty( "EgammaCalibrationAndSmearingTool", m_egammaCalibTool);
	  //
	  declareProperty( "TauSelectionTool", m_tauSelTool);
	  declareProperty( "TauSmearingTool", m_tauSmearingTool);
	  declareProperty( "TauEfficiencyCorrectionsTool", m_tauEffTool);
	  //
	  declareProperty( "BTaggingEfficiencyTool", m_btagEffTool);
	  ///////declareProperty( "BTaggingSelectionTool", m_btagSelTool);
	  ///////declareProperty( "BTaggingSelectionTool_OR", m_btagSelTool_OR);
	  //
	  declareProperty( "METMaker", m_metMaker);
	  declareProperty( "METSystTool", m_metSystTool);
	  //
	  declareProperty( "TrigConfigTool", m_trigConf );
	  declareProperty( "TrigDecisionTool", m_trigDec );
	  declareProperty( "EgammaTrigMatchTool", m_EgammaMatchTool);
	  declareProperty( "MuonTrigMatchTool", m_MuonMatchTool );
	  declareProperty( "TauTrigMatchTool", m_TauMatchTool );
	  //
	  declareProperty( "IsolationSelectionTool", m_isoTool);
	  //
	  declareProperty( "PileupReweightingTool", m_prwTool);
     **/
	
	}
  
  StatusCode DVObjDef_xAOD::initialize() {
    return StatusCode::SUCCESS;
  }
  
  template <typename T>
  static void DeleteTool(ToolHandle<T>& handle)
  {
    if (handle.empty())
      return;
    
    std::string name = handle.name();
    if (asg::ToolStore::contains<T>(name)) {
      auto tool = asg::ToolStore::get<T>(name);
      delete tool;
    }
  }
  
  DVObjDef_xAOD::~DVObjDef_xAOD() {
    
    // remove all tools from the asg::ToolStore (and delete them)
    // so that they don't get re-used if we set up another SUSYTools
    // instance, e.g. when processing two datasets in one EventLoop
    // job
    
    /**
    DeleteTool(m_jetCalibTool);
    DeleteTool(m_jerTool);
    DeleteTool(m_jerSmearingTool);
    DeleteTool(m_jetUncertaintiesTool);
    DeleteTool(m_jetCleaningTool);
    //
    DeleteTool(m_muonSelectionTool);
    DeleteTool(m_muonCalibrationAndSmearingTool);
    DeleteTool(m_muonEfficiencySFTool);
    DeleteTool(m_muonIsolationSFTool);
    DeleteTool(m_muonTriggerSFTool);
    //
    DeleteTool(m_elecEfficiencySFTool_reco);
    DeleteTool(m_elecEfficiencySFTool_id);
    DeleteTool(m_elecEfficiencySFTool_trig);
    DeleteTool(m_egammaCalibTool);
    DeleteTool(m_elecSelLikelihood);
    DeleteTool(m_elecSelIsEM);
    DeleteTool(m_elecSelLikelihoodBaseline);
    DeleteTool(m_elecSelIsEMBaseline);
    DeleteTool(m_photonSelIsEM);
    DeleteTool(m_photonEfficiencySFTool);
    DeleteTool(m_electronPhotonShowerShapeFudgeTool);
    //
    DeleteTool(m_tauSelTool);
    DeleteTool(m_tauSmearingTool);
    DeleteTool(m_tauEffTool);
    //
    DeleteTool(m_btagEffTool);
    //
    DeleteTool(m_metMaker);
    DeleteTool(m_metSystTool);
    //
    DeleteTool(m_trigConf);
    DeleteTool(m_trigDec);
    DeleteTool(m_MuonMatchTool);
    DeleteTool(m_EgammaMatchTool);
    DeleteTool(m_TauMatchTool);
    //
    DeleteTool(m_isoTool);
    //
    DeleteTool(m_prwTool);
     **/
  }
  
} // end namespace DV
#endif