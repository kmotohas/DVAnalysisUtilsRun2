#include "DVTools/DVObjDef_xAOD.h"

using namespace DV;
//  using namespace std;


#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetResolution/JERTool.h"
#include "JetResolution/JERSmearingTool.h"
#include "JetUncertainties/JetUncertaintiesTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
//
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
//
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
#include "PhotonEfficiencyCorrection/AsgPhotonEfficiencyCorrectionTool.h"
//
#include "TauAnalysisTools/TauSelectionTool.h"
#include "TauAnalysisTools/TauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/TauSmearingTool.h"
//
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
//////////////#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
//
#include "METUtilities/METMaker.h"
#include "METUtilities/METSystematicsTool.h"
// xAOD trigger
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigEgammaMatchingTool/TrigEgammaMatchingTool.h"
#include "TrigMuonMatching/TrigMuonMatching.h"
#include "TrigTauMatching/TrigTauMatching.h"
//
#include "IsolationSelection/IsolationSelectionTool.h"
//
#include "PileupReweighting/PileupReweightingTool.h"

StatusCode DVObjDef_xAOD::DVToolsInit()
{
  return StatusCode::SUCCESS;
}