#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <DVTools/xAODEvtLoop.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/TStore.h"

#include "CPAnalysisExamples/xExampleUtils.h"

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "PathResolver/PathResolver.h"

// Root
#include "TEnv.h"
#include "TLorentzVector.h"

// this is needed to distribute the algorithm to the workers
ClassImp(xAODEvtLoop)

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )            \
  do {                                             \
    if( ! EXP.isSuccess() ) {                      \
      Error( CONTEXT,                              \
       XAOD_MESSAGE( "Failed to execute: %s" ),    \
       #EXP );                                     \
      return EL::StatusCode::FAILURE;              \
    }                                              \
  } while( false )

xAODEvtLoop :: xAODEvtLoop ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  
  Info("xAODEvtLoop()", "in xAODEvtLoop constructor");
}



EL::StatusCode xAODEvtLoop :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  
  Info("setupJob()", "in setupJob function of xAODEvtLoop");
  
  job.useXAOD ();
  // avoid having to manually remove "submitDir" each time
  job.options()->setDouble (EL::Job::optRemoveSubmitDir, 1);
  // let's initialize the algorithm to use the xAODRootAccess package
  //xAOD::Init( "xAODEventLoop" ).ignore(); // call before opening first file
  EL_RETURN_CHECK("setupJob()", xAOD::Init());
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODEvtLoop :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  
  Info("histInitialize()", "in histInitialize function of xAODEvtLoop");
  
  int nbinsR = 60; double minR = 0.; double maxR = 300.;
  int nbinsD0 = 60; double minD0 = 0.; double maxD0 = 300.;
  int nbinsZ0 = 300; double minZ0 = 0.; double maxZ0 = 1500.;
  int nbinsMeff = 200; double minMeff = 0.; double maxMeff = 1000.;
  int nbinsMET = 200; double minMET = 0.; double maxMET = 1000.;
  int nbinsMEToverMeff = 22; double minMEToverMeff = 0.; double maxMEToverMeff = 1.1;
  int nbinsJetPt = 100; double minJetPt = 0.; double maxJetPt = 500.;
  int nbinsNJets = 15; double minNJets = 0.; double maxNJets = 15.;
  // basic plots
  h_JetPt = new TH1D("h_JetPt","Jet Pt;Jet Pt [GeV]", nbinsJetPt,minJetPt,maxJetPt);
  wk()->addOutput(h_JetPt);
  h_nJets = new TH1D("h_nJets","nJets;nJets", nbinsNJets,minNJets,maxNJets);
  wk()->addOutput(h_nJets);
  h_nJets10 = new TH1D("h_nJets10","nJets10;nJets with Pt > 10 GeV", nbinsNJets,minNJets,maxNJets);
  wk()->addOutput(h_nJets10);
  h_nJets20 = new TH1D("h_nJets20","nJets20;nJets with Pt > 20 GeV", nbinsNJets,minNJets,maxNJets);
  wk()->addOutput(h_nJets20);
  h_chi0pt = new TH1D("h_chi0pt","chi0pt;Lightest Neutralino Pt [GeV]", nbinsMET,minMET,maxMET);
  wk()->addOutput(h_chi0pt);
  if(m_doVertexingEfficiency){
    h_recoVertPosR = new TH1D("h_recoVertPosR","Reconstructed Vertex Position in R;R [mm]", nbinsR,minR,maxR);
    wk()->addOutput(h_recoVertPosR);
    h_truthVertPosR = new TH1D("h_truthVertPosR","Truth Vertex Position in R;R [mm]", nbinsR,minR,maxR);
    wk()->addOutput(h_truthVertPosR);
    g_vertEffR = nullptr;
  }
  if(m_doTrackingEfficiency){
    h_recoTrackD0 = new TH1D("h_recoTrackD0","Reconstructed Track d0;d0 [mm]", nbinsD0,minD0,maxD0);
    wk()->addOutput(h_recoTrackD0);
    h_truthTrackD0 = new TH1D("h_truthTrackD0","Truth Track d0;d0 [mm]", nbinsD0,minD0,maxD0);
    wk()->addOutput(h_truthTrackD0);
    g_trackEffD0 = nullptr;
    h_recoTrackZ0 = new TH1D("h_recoTrackZ0","Reconstructed Track z0;z0 [mm]", nbinsZ0,minZ0,maxZ0);
    wk()->addOutput(h_recoTrackZ0);
    h_truthTrackZ0 = new TH1D("h_truthTrackZ0","Truth Track z0;z0 [mm]", nbinsZ0,minZ0,maxZ0);
    wk()->addOutput(h_truthTrackZ0);
    g_trackEffZ0 = nullptr;
  }
  if (m_doDESDFilterStudy){
    h_Meff = new TH1D("h_Meff","Effective Mass;Effective Mass [GeV]",nbinsMeff,minMeff,maxMeff);
    wk()->addOutput(h_Meff);
    h_MET_Calo = new TH1D("h_MET_Calo","MET_Calo;MET_Calo [GeV]",nbinsMET,minMET,maxMET);
    wk()->addOutput(h_MET_Calo);
    h_METoverMeff = new TH1D("h_METoverMeff","MET over Meff;MET_Calo/Meff",nbinsMEToverMeff,minMEToverMeff,maxMEToverMeff);
    wk()->addOutput(h_METoverMeff);
    h_METvsMeff = new TH2D("h_METvsMeff","MET vs Meff;Meff [GeV]; MET_Calo [GeV]",
                           nbinsMeff,minMeff,maxMeff, nbinsMET,minMET,maxMET);
    wk()->addOutput(h_METvsMeff);
    h_METvsChi0Pt = new TH2D("h_METvsChi0Pt","MET vs Chi0Pt;Chi0 Pt [GeV]; MET_Calo [GeV]",
                           nbinsMET,minMET,maxMET, nbinsMET,minMET,maxMET);
    wk()->addOutput(h_METvsChi0Pt);
  }
  if (m_doTriggerStudy){
    h_HLT_xe80 = new TH1D("h_HLT_xe80","HLT_xe80;True/False",2,0,2);
    wk()->addOutput(h_HLT_xe80);
    h_HLT_xe100 = new TH1D("h_HLT_xe100","HLT_xe100;True/False",2,0,2);
    wk()->addOutput(h_HLT_xe100);
    h_HLT_xe100_tc_lcw = new TH1D("h_HLT_xe100_tc_lcw","HLT_xe100_tc_lcw;True/False",2,0,2);
    wk()->addOutput(h_HLT_xe100_tc_lcw);
    h_HLT_xe100_tc_lcw_wEFMu = new TH1D("h_HLT_xe100_tc_lcw_wEFMu","HLT_xe100_tc_lcw_wEFMu;True/False",2,0,2);
    wk()->addOutput(h_HLT_xe100_tc_lcw_wEFMu);
    h_HLT_xe100_wEFMu = new TH1D("h_HLT_xe100_wEFMu","HLT_xe100_wEFMu;True/False",2,0,2);
    wk()->addOutput(h_HLT_xe100_wEFMu);
    h_HLT_j100_xe80 = new TH1D("h_HLT_j100_xe80","HLT_j100_xe80;True/False",2,0,2);
    wk()->addOutput(h_HLT_j100_xe80);
    h_HLT_xe100_OR = new TH1D("h_HLT_xe100_OR","HLT_xe100_OR;True/False",2,0,2);
    wk()->addOutput(h_HLT_xe100_OR);
    h_HLT_jetPlusMETvsMET = new TH2D("h_HLT_jetPlusMETvsMET","HLT_jetPlusMETvsMET;MET [True/False];jetPlusMET [True/False]",
                                     2,0,2,2,0,2);
    wk()->addOutput(h_HLT_jetPlusMETvsMET);
  }
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODEvtLoop :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  
  Info("fileExecute()", "in fileExecute function of xAODEvtLoop");
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODEvtLoop :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  
  Info("changeInput()", "in changeInput function of xAODEvtLoop");
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODEvtLoop :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  
  Info("initialize()", "in initialize function of xAODEvtLoop");
  
  m_event = wk()->xaodEvent();
  m_eventCounter = 0;
  
  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", m_event->getEntries() ); // print long long int
  
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("execute", m_event->retrieve(eventInfo, "EventInfo"));
  
  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  if(eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)){
    m_isMC = true; // can do something with this later
  }
  
  m_configFile = "DVTools/xAODEvtLoop.conf";
  m_configFile = (PathResolverFindCalibFile(m_configFile)).c_str();
  readConfig();
  
  // initialize and configure trigger tools
  m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
  EL_RETURN_CHECK("initialize", m_trigConfigTool->initialize());
  ToolHandle<TrigConf::ITrigConfigTool> trigConfigHandle( m_trigConfigTool );
  m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDicisionTool");
  // connect the TrigDicisionTool to the ConfigTool
  EL_RETURN_CHECK("initialize", m_trigDecisionTool->setProperty("ConfigTool", trigConfigHandle));
  EL_RETURN_CHECK("initialize", m_trigDecisionTool->setProperty("TrigDecisionKey", "xTrigDecision"));
  EL_RETURN_CHECK("initialize", m_trigDecisionTool->initialize());
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODEvtLoop :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  m_event = wk()->xaodEvent();
  if( (m_eventCounter % 100) == 0 ) Info("execute()", "Event number = %lu", m_eventCounter );
  m_eventCounter++;
  
  // retrieve containers
  const xAOD::VertexContainer* recoVertices = 0;
  EL_RETURN_CHECK("execute()", m_event->retrieve(recoVertices, m_vertexContainer));
  const xAOD::TrackParticleContainer* recoTracks = 0;
  EL_RETURN_CHECK("execute()", m_event->retrieve(recoTracks, m_trackContainer));
  const xAOD::MissingETContainer* MET_Calo = 0;
  EL_RETURN_CHECK("execute()", m_event->retrieve(MET_Calo, "MET_Calo"));
  const xAOD::MissingETContainer* MET_LocHadTopo = 0;
  EL_RETURN_CHECK("execute()", m_event->retrieve(MET_LocHadTopo, "MET_LocHadTopo"));
  const xAOD::JetContainer* jets = 0;
  EL_RETURN_CHECK("execute()", m_event->retrieve(jets, m_jetContainer));
  const xAOD::TruthParticleContainer* truthParticles = 0;
  //const xAOD::MissingETContainer* truthMETs = 0;
  if(m_isMC){
    EL_RETURN_CHECK("execute()", m_event->retrieve(truthParticles, "TruthParticles"));
    //EL_RETURN_CHECK("execute()", m_event->retrieve(truthMETs, "MET_Truth"));
  }
  
  //==================
  // basic plots
  //==================
  int nJets = 0;
  int nJets10 = 0;
  int nJets20 = 0;
  for (const auto& jet : *jets){
    h_JetPt->Fill(jet->pt() * 0.001);
    nJets++;
    if (jet->pt() * 0.001 > 10.) nJets10++;
    if (jet->pt() * 0.001 > 20.) nJets20++;
  }
  h_nJets->Fill(nJets);
  h_nJets10->Fill(nJets10);
  h_nJets20->Fill(nJets20);
  
  // get the neutralino pair pt since MET_Truth is not reliable for R-hadron sample at all
  double chi0pt = 0.;
  int nChi0 = 0;
  if(m_isMC){
    TLorentzVector tlv_chi0_1;
    TLorentzVector tlv_chi0_2;
    for(const auto& truthParticle : *truthParticles){
      //if(isLightestNeutralino( truthParticle->pdgId() ) ) {
      //  std::cout << "LSP status: " << truthParticle->status() << std::endl;
      //}
      //if(isLLP(truthParticle->pdgId()) && truthParticle->hasDecayVtx()){ // gluino
      //  std::vector< const xAOD::TruthParticle* > tmp_dscd;
      //  getDescendants(truthParticle, tmp_dscd); // defined in CPAnalysisExamples/xExampleUtils.h
      //  for (const auto& dscd : tmp_dscd) {
      //    std::cout << "m_eventCounter: " << m_eventCounter << ", descendant pdgid " << dscd->pdgId() << std::endl;
      //    if(isLLP(dscd->pdgId())){
      //      std::vector< const xAOD::TruthParticle* > tmp_dscd2;
      //      getDescendants(dscd, tmp_dscd2); // defined in CPAnalysisExamples/xExampleUtils.h
      //      for (const auto& dscd2 : tmp_dscd2) {
      //        std::cout << "m_eventCounter: " << m_eventCounter << ", descendant2 pdgid " << dscd2->pdgId() << std::endl;
      //      }
      //      
      //    }
      //  }
      //  //for(size_t ii = 0; ii < truthParticle->decayVtx()->nOutgoingParticles(); ii++){
      //  //  std::cout << "m_eventCounter: " << m_eventCounter << ", outgoing pdgid " << truthParticle->decayVtx()->outgoingParticle(ii)->pdgId() << std::endl;
      //  //}
      //}
      if(isLightestNeutralino( truthParticle->pdgId() ) && truthParticle->status() == 1) {
        nChi0++;
        if (nChi0 == 1) {
          tlv_chi0_1.SetPtEtaPhiM(truthParticle->pt(), truthParticle->eta(), truthParticle->phi(), truthParticle->m() );
        } else if (nChi0 == 2){
          tlv_chi0_2.SetPtEtaPhiM(truthParticle->pt(), truthParticle->eta(), truthParticle->phi(), truthParticle->m() );
        } else {
          Warning("execute()", "nChi0 is greater than 2");
        }
      } // end if final state LSP
    } // end for loop over truthParticles
    if (nChi0 != 2) {
      Warning("execute()", "m_eventCounter = %lu, nChi0 = %d, is not equal to 2.", m_eventCounter, nChi0);
    }
    chi0pt = (tlv_chi0_1+tlv_chi0_2).Pt();
    h_chi0pt->Fill(chi0pt * 0.001);
  } // end if isMC
  
  //===================
  // each analysis
  //===================
  if(m_doVertexingEfficiency){
    for(const auto& truthParticle : *truthParticles){
      if(isLLP(truthParticle->pdgId()) && truthParticle->hasDecayVtx()){
        double truthVertX = truthParticle->decayVtx()->x();
        double truthVertY = truthParticle->decayVtx()->y();
        double truthVertZ = truthParticle->decayVtx()->z();
        double truthVertR = TMath::Sqrt(truthVertX * truthVertX + truthVertY * truthVertY);
        if (nChi0 != 2) {
          Warning("execute()", "truth position of decay vertex of LLP in R is %f mm", truthVertR);
        }
        h_truthVertPosR->Fill(truthVertR);
        for(const auto& recoVertex : *recoVertices){
          double recoVertX = recoVertex->x();
          double recoVertY = recoVertex->y();
          double recoVertZ = recoVertex->z();
          //double recoVertR = recoVertX * recoVertX + recoVertY * recoVertY;
          if (getDistance3D(truthVertX, truthVertY, truthVertZ,
                            recoVertX, recoVertY, recoVertZ) < 10.) { // if distance < 1 cm
            h_recoVertPosR->Fill(truthVertR);
            break;
          }
        } // end loop over recoVertices
      } // endl if isLLP && hasDecayVtx
    } // end loop over truthParticles
  } // end if m_doVertexingEfficiency
  
  if(m_doTrackingEfficiency){
    for(const auto& truthTrack : *truthParticles){
      if(truthTrack->status() == 1 && truthTrack->isCharged()
         && truthTrack->pt() > 1 * 0.001 && TMath::Abs(truthTrack->eta()) < 2.5) {
        //h_truthTrackD0->Fill(truthTrack->d0());
        //h_truthTrackZ0->Fill(truthTrack->z0());
      }
    }
    for(const auto& recoTrack : *recoTracks){
      h_recoTrackD0->Fill(recoTrack->d0());
      h_recoTrackZ0->Fill(recoTrack->z0());
    }
  }
  
  if (m_doDESDFilterStudy) {
    // This part is for low mass DV search
    double DESD_MET = 0;
    double DESD_MET_x = 0;
    double DESD_MET_y = 0;
    //if (MET_Calo->size() == 1) {
    //  DESD_MET = MET_Calo->at(0)->met();
    //}
    //debug
    //int counter = 0;
    for (const auto& met_calo : *MET_Calo){ // MET_Calo->size() should be 7
      //std::cout << "evt: " << m_eventCounter << ", index: " << counter << ", met: " << met->met() << std::endl;
      DESD_MET_x += met_calo->met() * TMath::Cos(met_calo->phi());
      DESD_MET_y += met_calo->met() * TMath::Sin(met_calo->phi());
      //break; // same as MET_Calo->at(0)->met();
      //counter++;
      /**
      1. EMB - LAr EM barrel
      2. EME - LAr EM endcap
      3. FCAL - Forward calorimeter
      4. HEC - LAr hadronic endcap
      5. PEMB - LAr EM barrel presampler
      6. PEME - LAr EM barrel presampler
      7. Tile - Tile hadronic barrel
      https://twiki.cern.ch/twiki/bin/view/AtlasProtected/Run2xAODMissingET#MET_Calo_container
      */
    }
    DESD_MET = TMath::Sqrt(DESD_MET_x * DESD_MET_x + DESD_MET_y * DESD_MET_y);
    double totalJetPt = 0;
    for (const auto& jet : *jets) {
      if (( jet->pt() * 0.001 < m_jetPtCut) || (fabs(jet->eta())>m_jetEtaCut))  continue;
      totalJetPt += jet->pt();
    }
    double Meff = 0;
    Meff += DESD_MET;
    Meff += totalJetPt;
    ///     msg(MSG::DEBUG)<<" MET "<< MET<< " totalJetPT "<<totalJetPT<<" Meff "<<Meff<<" ratio "<< MET/Meff <<endreq;
    
    h_Meff->Fill(Meff * 0.001);
    h_METoverMeff->Fill(DESD_MET / Meff);
    h_MET_Calo->Fill(DESD_MET * 0.001);
    h_METvsMeff->Fill(Meff * 0.001, DESD_MET * 0.001);
    h_METvsChi0Pt->Fill(chi0pt * 0.001, DESD_MET * 0.001);
    
    /**
    if ((Meff > m_MeffCut) || ((DESD_MET > m_METCut) && (DESD_MET/Meff > m_METoverMeffLowerCut )))  {   //// NOTE: OR of these two requirements
      passesEvent=true;
      ++m_npass;
    }
    */
  }
  
  if (m_doTriggerStudy){
    // https://twiki.cern.ch/twiki/bin/view/Atlas/TrigDecisionTool
    // examine the HLT_xe80* chains, see if they passed/failed and their total prescale
    //auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_xe80.*");
    auto chainGroup = m_trigDecisionTool->getChainGroup(m_triggerCollection);
    //std::map<std::string, int> triggerCounts;
    bool isPassed_xe100_OR = false;
    bool isPassed_j100_xe80 = false;
    for (auto &trig : chainGroup->getListOfTriggers()) {
      auto cg = m_trigDecisionTool->getChainGroup(trig);
      std::string thisTrig = trig;
      //Info("execute()","%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f",
      //     thisTrig.c_str(),cg->isPassed(),cg->getPrescale());
      if (thisTrig == "HLT_xe80") {
        h_HLT_xe80->Fill(cg->isPassed());
      } else if (thisTrig == "HLT_xe100") {
        h_HLT_xe100->Fill(cg->isPassed());
        if(cg->isPassed() == true) isPassed_xe100_OR = true;
      } else if (thisTrig == "HLT_xe100_tc_lcw") {
        h_HLT_xe100_tc_lcw->Fill(cg->isPassed());
        if(cg->isPassed() == true) isPassed_xe100_OR = true;
      } else if (thisTrig == "HLT_xe100_tc_lcw_wEFMu") {
        h_HLT_xe100_tc_lcw_wEFMu->Fill(cg->isPassed());
        if(cg->isPassed() == true) isPassed_xe100_OR = true;
      } else if (thisTrig == "HLT_xe100_wEFMu") {
        h_HLT_xe100_wEFMu->Fill(cg->isPassed());
        if(cg->isPassed() == true) isPassed_xe100_OR = true;
      } else if (thisTrig == "HLT_j100_xe80") {
        h_HLT_j100_xe80->Fill(cg->isPassed());
        if(cg->isPassed() == true) isPassed_j100_xe80 = true;
      }
    } // end for loop (c++11 style) over chain group matching
    h_HLT_xe100_OR->Fill(isPassed_xe100_OR);
    h_HLT_jetPlusMETvsMET->Fill(isPassed_xe100_OR, isPassed_j100_xe80);
  }
  
  if (nChi0 == 2) {
    m_nEventsChi0_2++;
  } else if (nChi0 == 1) {
    m_nEventsChi0_1++;
  } else if (nChi0 == 0) {
    m_nEventsChi0_0++;
  }
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODEvtLoop :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODEvtLoop :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  
  Info("finalize()", "in finalize function of xAODEvtLoop");
  
  m_event = wk()->xaodEvent();
  
  if(m_doVertexingEfficiency){
    g_vertEffR = new TGraphAsymmErrors(h_recoVertPosR,h_truthVertPosR);
    g_vertEffR->SetName("g_vertEffR");
    g_vertEffR->SetTitle("vertexing Efficiency in R;R [mm]; Efficiency");
    wk()->addOutput(g_vertEffR);
  }
  
  std::cout << "m_nEventsChi0_2 " << m_nEventsChi0_2 << "\n"
            << "m_nEventsChi0_1 " << m_nEventsChi0_1 << "\n"
            << "m_nEventsChi0_0 " << m_nEventsChi0_0 << std::endl;
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODEvtLoop :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode xAODEvtLoop :: readConfig() {
  Info("readConfig()","*****     *****     *****     *****" );
  Info("readConfig()", "Configuring from file %s", m_configFile.c_str() );
  
  TEnv rEnv;
  int success = -1;
  success = rEnv.ReadFile(m_configFile.c_str(), kEnvAll);
  if(success != 0) {
    Error("readConfig()", "Config file cannot be opened");
    return StatusCode::FAILURE;
  }
  
  Info("readConfig()", "Config file opened" );
  
  // basic configuration
  m_longlivedParticle = rEnv.GetValue( "longlivedParticle", "gluino" );
  m_vertexContainer = rEnv.GetValue( "vertexContainer", "RPVSecVertices" );
  m_trackContainer = rEnv.GetValue( "trackContainer", "InDetTrackParticles" );
  m_jetContainer = rEnv.GetValue( "jetContainer", "AntiKt4EMTopoJets" );
  Info("readConfig()", "m_longlivedParticle is set as %s", m_longlivedParticle.c_str());
  Info("readConfig()", "m_vertexContainer is set as %s", m_vertexContainer.c_str());
  Info("readConfig()", "m_trackContainer is set as %s", m_trackContainer.c_str());
  Info("readConfig()", "m_jetContainer is set as %s", m_jetContainer.c_str());
  // cut config
  m_jetPtCut = rEnv.GetValue( "jetPtCut", 20. );
  m_jetEtaCut = rEnv.GetValue( "jetEtaCut", 4.5 );
  m_MeffCut = rEnv.GetValue( "MeffCut", 100. );
  m_METoverMeffLowerCut = rEnv.GetValue( "METoverMeffLowerCut", 0.3 );
  m_METoverMeffUpperCut = rEnv.GetValue( "METoverMeffUpperCut", 0.7 );
  m_METCut = rEnv.GetValue( "METCut", 80. );
  Info("readConfig()", "m_jetPtCut is set as %f", m_jetPtCut);
  Info("readConfig()", "m_jetEtaCut is set as %f", m_jetEtaCut);
  Info("readConfig()", "m_MeffCut is set as %f", m_MeffCut);
  Info("readConfig()", "m_METoverMeffLowerCut is set as %f", m_METoverMeffLowerCut);
  Info("readConfig()", "m_METoverMeffUpperCut is set as %f", m_METoverMeffUpperCut);
  Info("readConfig()", "m_METCut is set as %f", m_METCut);
  // which analysis to be done
  m_doVertexingEfficiency = rEnv.GetValue( "doVertexingEfficiency", false );
  m_doTrackingEfficiency = rEnv.GetValue( "doTrackingEfficiency", false );
  m_doDESDFilterStudy = rEnv.GetValue( "doDESDFilterStudy", false );
  m_doTriggerStudy = rEnv.GetValue( "doTriggerStudy", false );
  Info("readConfig()", "m_doVertexingEfficiency is set as %d", m_doVertexingEfficiency);
  Info("readConfig()", "m_doTrackingEfficiency is set as %d", m_doTrackingEfficiency);
  Info("readConfig()", "m_doDESDFilterStudy is set as %d", m_doDESDFilterStudy);
  Info("readConfig()", "m_doTriggerStudy is set as %d", m_doTriggerStudy);
  // trigger collection
  m_triggerCollectionName = rEnv.GetValue( "triggerCollectionName", "jetPlusMET" );
  Info("readConfig()", "m_triggerCollectionName is set as %s", m_triggerCollectionName.c_str());
  
  Info("readConfig()","*****     *****     *****     *****" );
  
  if (m_triggerCollectionName == "jetPlusMET") {
    m_triggerCollection.push_back("HLT_j100_xe80");
  }
  if (m_triggerCollectionName == "jetPlusMET" || m_triggerCollectionName == "MET") {
    m_triggerCollection.push_back("HLT_xe100");
    m_triggerCollection.push_back("HLT_xe100_tc_lcw");
    m_triggerCollection.push_back("HLT_xe100_tc_lcw_wEFMu");
    m_triggerCollection.push_back("HLT_xe100_wEFMu");
  }
  
  return EL::StatusCode::SUCCESS;
}