#include <DVTools/xAODEvtLoop.h>
#include <DVTools/IDVObjDef_xAODTool.h>
#include <DVTools/DVObjDef_xAOD.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class IDVObjDef_xAODTool+;
#pragma link C++ class DVObjDef_xAOD+;
#pragma link C++ class xAODEvtLoop+;

#pragma link C++ class std::vector< std::vector<long> >; // necessary to read RPVSecVertices
#endif
